# -*- coding: utf-8 -*-
###############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2011-Today Serpent Consulting Services PVT LTD
#    (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

from datetime import datetime
from odoo import models, fields, api, _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.exceptions import Warning


class account_asset_asset(models.Model):
    _inherit = 'account.asset.asset'
    _description = 'Asset'

    sale_date = fields.Date('Sale Date', help='Sale Date of the Property.')
    sale_price = fields.Float('Sale Price', help='Sale price of the Property.')
#   country_id = fields.Many2one('res.country', 'Country', ondelete='restrict')
    payment_term = fields.Many2one('account.payment.term', 'Payment Terms')
    sale_cost_ids = fields.One2many('sale.cost', 'sale_property_id', 'Costs')
    customer_id = fields.Many2one('res.partner', 'Customer')
    end_date = fields.Date('End Date')
    purchase_price = fields.Float('Purchase Price',
                                  help='Purchase price of the Property.')
    multiple_owners = fields.Boolean('Multiple Owners',
                                     help="Check this box if there is \
                                     multiple Owner of the Property.")
    total_owners = fields.Integer('Number of Owners')
    recurring_rule_type = fields.Selection([('monthly', 'Month(s)')],
                                           'Recurrency', default='monthly',
                                           help="Invoice automatically repeat \
                                           at specified interval.")
    purchase_cost_ids = fields.One2many(
        'cost.cost', 'purchase_property_id', 'Costs')
    note = fields.Text('Notes', help='Additional Notes.')
    return_period = fields.Float(compute='calc_return_period',
                                 string="Return Period(In Months)", store=True,
                                 help='Average of Purchase Price \
                                 and Ground Rent.')

    @api.one
    @api.depends('purchase_price', 'ground_rent')
    def calc_return_period(self):
        """
        This Method is used to Calculate Return Period.
        @param self: The object pointer
        @return: Calculated Return Period.
        """
        rtn_prd = 0
        if self.ground_rent != 0 and self.purchase_price != 0:
            rtn_prd = self.purchase_price / self.ground_rent
        self.return_period = rtn_prd

    @api.multi
    def create_purchase_installment(self):
        """
        This Button method is used to create purchase installment
        information entries.
        @param self: The object pointer
        """
        year_create = []
        for res in self:
            amount = res['purchase_price']
            if res['purchase_price'] == 0.0:
                raise Warning(_('Please Enter Valid Purchase Price'))
            starting_date_date = datetime.strptime(
                res['date'], DEFAULT_SERVER_DATE_FORMAT)
            starting_day = datetime.strptime(
                res['date'], DEFAULT_SERVER_DATE_FORMAT).day
            if not res['end_date']:
                raise Warning(_('Please Select End Date'))
            ending_date_date = datetime.strptime(
                res['end_date'], DEFAULT_SERVER_DATE_FORMAT)
            ending_day = datetime.strptime(
                res['end_date'], DEFAULT_SERVER_DATE_FORMAT).day
            if ending_date_date.date() < starting_date_date.date():
                raise Warning(
                    _("Please Select End Date greater than purchase date"))
    #        method used to calculate difference in month between two dates

            def diff_month(d1, d2):
                return (d1.year - d2.year) * 12 + d1.month - d2.month
            difference_month = diff_month(ending_date_date, starting_date_date)
            if difference_month == 0:
                amnt = amount
            else:
                if ending_date_date.day > starting_date_date.day:
                    difference_month += 1
                amnt = amount / difference_month
            cr = self._cr
            cr.execute(
                "SELECT date FROM cost_cost WHERE purchase_property_id=%s" % \
                self._ids[0])
            exist_dates = cr.fetchall()
            date_add = self.date_addition(
                res['date'], res['end_date'], res['recurring_rule_type'])
            exist_dates = map(lambda x: x[0], exist_dates)
            result = list(set(date_add) - set(exist_dates))
            result.sort(key=lambda item: item, reverse=False)
            ramnt = amnt
            remain_amnt = 0.0
            for dates in result:
                remain_amnt = amount - ramnt
                remain_amnt_per = (remain_amnt / res['purchase_price']) * 100
                if remain_amnt < 0:
                    remain_amnt = remain_amnt * -1
                if remain_amnt_per < 0:
                    remain_amnt_per = remain_amnt_per * -1
                year_create.append((0, 0, {
                    'currency_id': res.currency_id.id or False,
                    'date': dates,
                    'purchase_property_id': self._ids[0],
                    'amount': amnt,
                    'remaining_amount': remain_amnt,
                    'rmn_amnt_per': remain_amnt_per,
                }))
                amount = remain_amnt
        return self.write({
                           'purchase_cost_ids': year_create,
                           'pur_instl_chck': True
                           })

    @api.multi
    def genrate_payment_enteries(self):
        """
        This Button method is used to generate property sale payment entries.
        @param self: The object pointer
        """
        for data in self:
            amount = data.sale_price
            year_create = []
            payment_term_brw = self.env[
                'account.payment.term'].browse(data.payment_term.id)
            pterm_list = payment_term_brw.compute(
                data.sale_price, data.sale_date)
            if amount == 0.0:
                raise Warning(_('Please Enter Valid Sale Price'))
            rmnt = 0.0
            for line in pterm_list:
                lst = list(line[0])
                remain_amnt = amount - lst[1]
                remain_amnt_per = (remain_amnt / data.sale_price) * 100
                if remain_amnt < 0:
                    remain_amnt = remain_amnt * -1
                if remain_amnt_per < 0:
                    remain_amnt_per = remain_amnt_per * -1
                year_create.append((0, 0, {
                    'currency_id': data.currency_id.id or False,
                    'date': lst[0],
                    'sale_property_id': self._ids,
                    'amount': lst[1],
                    'remaining_amount': remain_amnt,
                    'rmn_amnt_per': remain_amnt_per,
                }))
                amount = amount - lst[1]
            self.write({
                        'sale_cost_ids': year_create,
                        'sale_instl_chck': True
                        })
        return True
