# -*- coding: utf-8 -*-
##############################################################################
#
#   OpenERP, Open Source Management Solution
#   Copyright (C) 2011-Today Serpent Consulting Services PVT LTD
#   (<http://www.serpentcs.com>)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
############################################################################
from datetime import datetime
from odoo.exceptions import Warning, except_orm
from odoo import models, fields, api, _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT


class TenantPartner(models.Model):
    _name = "tenant.partner"
    _inherits = {'res.partner': 'parent_id'}

    doc_name = fields.Char('Filename')
    id_attachment = fields.Binary('Identity Proof')
    tenancy_ids = fields.One2many(
        'account.analytic.account', 'tenant_id', 'Tenancy Details', help='Tenancy Details')
    parent_id = fields.Many2one(
        'res.partner', 'Partner', required=True, select=True, ondelete='cascade')
    tenant_ids = fields.Many2many('tenant.partner', 'agent_tenant_rel', 'agent_id', 'tenant_id', 'Tenant Details', domain=[
                                  ('customer', '=', True), ('agent', '=', False)])

    @api.model
    def create(self, vals):
        """
        This Method is used to overrides orm create method.
        @param self: The object pointer
        @param vals: dictionary of fields value.
        """
        dataobj = self.env['ir.model.data']
        property_user = False
        res = super(TenantPartner, self).create(vals)
        create_user = self.env['res.users'].create({'login': vals.get('email'), 'name': vals.get('name'),
                                                    'tenant_id': res.id, 'partner_id': res.parent_id.id})
        if res.customer:
            property_user = dataobj.get_object_reference(
                'property_management', 'group_property_user')
        if res.agent:
            property_user = dataobj.get_object_reference(
                'property_management', 'group_property_agent')
        if property_user:
            grop_id = self.env['res.groups'].browse(property_user[1])
            grop_id.write({'users': [(4, create_user.id)]})
        return res

    @api.model
    def default_get(self, fields):
        """
        This method is used to gets default values for tenant.
        @param self: The object pointer
        @param fields: Names of fields.
        @return: Dictionary of values.
        """
        context = dict(self._context or {})
        res = super(TenantPartner, self).default_get(fields)
        if context.get('tenant', False):
            res.update({'tenant': context['tenant']})
        res.update({'customer': True})
        return res

    @api.multi
    def unlink(self):
        """
            Overrides orm unlink method.
        @param self: The object pointer
        @return: True/False.
        """
        for tenant_rec in self:
            if tenant_rec.parent_id and tenant_rec.parent_id.id:
                releted_user = tenant_rec.parent_id.id
                new_user_rec = self.env['res.users'].search(
                    [('partner_id', '=', releted_user)])
                if releted_user and new_user_rec and new_user_rec.ids:
                    new_user_rec.unlink()
        return super(TenantPartner, self).unlink()


class PropertyType(models.Model):
    _name = "property.type"

    name = fields.Char('Name', size=50, required=True)


class RentType(models.Model):
    _name = "rent.type"
    _order = 'sequence_in_view'

    @api.multi
    @api.depends('name', 'renttype')
    def name_get(self):
        res = []
        for rec in self:
            rec_str = ''
            if rec.name:
                rec_str += rec.name
            if rec.renttype:
                # rec_str += ' ' + rec.renttype + '(S)'
                rec_str += ' ' + rec.renttype
            res.append((rec.id, rec_str))
        return res

    @api.model
    def name_search(self, name='', args=[], operator='ilike', limit=100):
        args += ['|', ('name', operator, name), ('renttype', operator, name)]
        cuur_ids = self.search(args, limit=limit)
        return cuur_ids.name_get()

    name = fields.Selection([('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'),
                             ('5', '5'), ('6', '6'), ('7', '7'), ('8', '8'),
                             ('9', '9'), ('10', '10'), ('11', '11'),
                             ('12', '12')
                             ])

    renttype = fields.Selection(
        [('Monthly', 'Monthly'), ('Yearly', 'Yearly'), ('Weekly', 'Weekly')])
    sequence_in_view = fields.Integer('Sequence')


class RoomType(models.Model):
    _name = "room.type"

    name = fields.Char('Name', size=50, required=True)


class Utility(models.Model):
    _name = "utility"

    name = fields.Char('Name', size=50, required=True)


class PlaceType(models.Model):
    _name = 'place.type'

    name = fields.Char('Place Type', size=50, required=True)


class MaintenanceType(models.Model):
    _name = 'maintenance.type'

    name = fields.Char('Maintenance Type', size=50, required=True)


class PropertyPhase(models.Model):
    _name = "property.phase"

    end_date = fields.Date('End Date')
    start_date = fields.Date('Beginning Date')
    commercial_tax = fields.Float('Commercial Tax (in %)')
    occupancy_rate = fields.Float('Occupancy Rate (in %)')
    lease_price = fields.Float('Sales/lease Price Per Month')
    phase_id = fields.Many2one('account.asset.asset', string='Property')
    operational_budget = fields.Float('Operational Budget (in %)')
    company_income = fields.Float('Company Income Tax CIT (in %)')


class PropertyPhoto(models.Model):
    _name = "property.photo"

    photos = fields.Binary('Photos')
    doc_name = fields.Char('Filename')
    photos_description = fields.Char('Description')
    photo_id = fields.Many2one('account.asset.asset', string='Property')


class PropertyRoom(models.Model):
    _name = "property.room"

    note = fields.Text('Notes')
    width = fields.Float('Width')
    height = fields.Float('Height')
    length = fields.Float('Length')
    image = fields.Binary('Picture')
    name = fields.Char('Name', size=60)
    attach = fields.Boolean('Attach Bathroom')
    type_id = fields.Many2one('room.type', 'Room Type')
    assets_ids = fields.One2many('room.assets', 'room_id', string='Assets')
    property_id = fields.Many2one('account.asset.asset', 'Property')


class NearbyProperty(models.Model):
    _name = 'nearby.property'

    distance = fields.Float('Distance (Km)')
    name = fields.Char('Name', size=100)
    type = fields.Many2one('place.type', 'Type')
    property_id = fields.Many2one('account.asset.asset', 'Property')


class PropertyMaintenace(models.Model):
    _name = "property.maintenance"
    _inherit = ['ir.needaction_mixin', 'mail.thread']

    date = fields.Date('Date', default=fields.Date.context_today)
    cost = fields.Float('Cost')
    type = fields.Many2one('maintenance.type', 'Type')
    assign_to = fields.Many2one('res.partner', 'Assign To')
    invc_id = fields.Many2one('account.invoice', 'Invoice')
    renters_fault = fields.Boolean('Renters Fault', default=True, copy=True)
    invc_check = fields.Boolean('Already Created', default=False)
    mail_check = fields.Boolean('Mail Send', default=False)
    property_id = fields.Many2one('account.asset.asset', 'Property')
    account_code = fields.Many2one('account.account', 'Account Code')
    notes = fields.Text('Description', size=100)
    name = fields.Selection([('Renew', 'Renew'), ('Repair', 'Repair')
                             ], string="Action", default='Repair')
    state = fields.Selection([('draft', 'Draft'), ('progress', 'In Progress'),
                              ('incomplete', 'Incomplete'), ('done', 'Done')],
                             'State', default='draft')

    @api.model
    def _needaction_domain_get(self):
        return [('state', '=', 'draft')]

    @api.model
    def create(self, values):
        res = super(PropertyMaintenace, self).create(values)
        if not res.renters_fault:
            res.write({'assign_to': res.property_id.property_manager.id})
        return res

    @api.multi
    def start_maint(self):
        """
        This Method is used to change maintenance state to progress.
        @param self: The object pointer
        """
        return self.write({'state': 'progress'})

    @api.multi
    def cancel_maint(self):
        """
        This Method is used to change maintenance state to incomplete.
        @param self: The object pointer
        """
        return self.write({'state': 'incomplete'})

    @api.multi
    def send_maint_mail(self):
        """
        This Method is used to send an email to assigned person.
        @param self: The object pointer
        """
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference(
                'property_management', 'mail_template_property_maintainance')[1]
        except ValueError:
            template_id = False
        for maint_rec in self:
            if not maint_rec.assign_to.email:
                raise except_orm(
                    ("Cannot send email: Assigned user has no email address."), maint_rec.assign_to.name)
            self.env['mail.template'].browse(template_id).send_mail(
                maint_rec.id, force_send=True, raise_exception=True)
        return self.write({'mail_check': True})

    @api.onchange('assign_to')
    def onchanchange_assign(self):
        for data in self:
            data.account_code = data.assign_to.property_account_payable_id

    @api.multi
    def create_invoice(self):
        """
        This Method is used to create invoice from maintenance record.
        @param self: The object pointer
        """
        for data in self:
            if not data.account_code:
                raise Warning(_("Please Select Account Code"))
            if not data.property_id.id:
                raise Warning(_("Please Select Property"))
            tncy_ids = self.env['account.analytic.account'].search(
                [('property_id', '=', data.property_id.id), ('state', '!=', 'close')])
            if len(tncy_ids.ids) == 0:
                raise Warning(_("No current tenancy for this property"))
            else:
                for tenancy_data in tncy_ids:
                    if data.renters_fault == True:
                        inv_line_values = {
                            'name': 'Maintenance For ' + data.type.name or "",
                            'origin': 'property.maintenance',
                            'quantity': 1,
                            'account_id': data.property_id.income_acc_id.id or False,
                            'price_unit': data.cost or 0.00,
                        }
                        inv_values = {
                            'origin': 'Maintenance For ' + data.type.name or "",
                            'type': 'out_invoice',
                            'property_id': data.property_id.id,
                            'partner_id': tenancy_data.tenant_id.parent_id.id or False,
                            'account_id': data.account_code.id or False,
                            'invoice_line_ids': [(0, 0, inv_line_values)],
                            'amount_total': data.cost or 0.0,
                            'date_invoice': datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT) or False,
                            'number': tenancy_data.name or '',
                        }

                    else:
                        inv_line_values = {
                            'name': 'Maintenance For ' + data.type.name or "",
                            'origin': 'property.maintenance',
                            'quantity': 1,
                            'account_id': data.property_id.income_acc_id.id or False,
                            'price_unit': data.cost or 0.00,
                        }
                        inv_values = {
                            'origin': 'Maintenance For ' + data.type.name or "",
                            'type': 'out_invoice',
                            'property_id': data.property_id.id,
                            'partner_id': tenancy_data.property_id.property_manager.id or False,
                            'account_id': tenancy_data.property_id.property_manager.property_account_payable_id.id or False,
                            'invoice_line_ids': [(0, 0, inv_line_values)],
                            'amount_total': data.cost or 0.0,
                            'date_invoice': datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT) or False,
                            'number': tenancy_data.name or '',
                        }

                    acc_id = self.env['account.invoice'].create(inv_values)
                    data.write(
                        {'renters_fault': False, 'invc_check': True, 'invc_id': acc_id.id, 'state': 'done'})
        return True

    @api.multi
    def open_invoice(self):
        """
        This Method is used to Open invoice from maintenance record.
        @param self: The object pointer
        """
        context = dict(self._context or {})
        wiz_form_id = self.env['ir.model.data'].get_object_reference(
            'account', 'invoice_form')[1]
        return {
            'view_type': 'form',
            'view_id': wiz_form_id,
            'view_mode': 'form',
            'res_model': 'account.invoice',
            'res_id': self.invc_id.id,
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': context,
        }


class CostCost(models.Model):
    _name = "cost.cost"
    _order = 'date'

    @api.one
    @api.depends('move_id')
    def _get_move_check(self):
        self.move_check = bool(self.move_id)

    date = fields.Date('Date')
    amount = fields.Float('Amount')
    name = fields.Char('Description', size=100)
    payment_details = fields.Char('Payment Details', size=100)
    currency_id = fields.Many2one('res.currency', 'Currency')
    move_id = fields.Many2one('account.move', 'Purchase Entry')
    purchase_property_id = fields.Many2one('account.asset.asset', 'Property')
    remaining_amount = fields.Float(
        'Remaining Amount', help='Shows remaining amount in currency')
    move_check = fields.Boolean(
        compute='_get_move_check', method=True, string='Posted', store=True)
    rmn_amnt_per = fields.Float(
        'Remaining Amount In %', help='Shows remaining amount in Percentage')

    @api.multi
    def create_move(self):
        """
        This button Method is used to create account move.
        @param self: The object pointer
        """
        context = dict(self._context or {})
        move_line_obj = self.env['account.move.line']
        created_move_ids = []
        journal_ids = self.env['account.journal'].search(
            [('type', '=', 'purchase')])
        for line in self:
            depreciation_date = datetime.now()
            company_currency = line.purchase_property_id.company_id.currency_id.id
            current_currency = line.purchase_property_id.currency_id.id
            sign = -1
            move_vals = {
                'name': line.purchase_property_id.code or False,
                'date': depreciation_date,
                'journal_id': journal_ids and journal_ids.ids[0],
                'asset_id': line.purchase_property_id.id or False,
                'source': line.purchase_property_id.name or False,
            }
            move_id = self.env['account.move'].create(move_vals)
            if not line.purchase_property_id.partner_id:
                raise Warning(_('Please Select Partner From General Tab'))
            move_line_obj.create({
                'name': line.purchase_property_id.name,
                'ref': line.purchase_property_id.code,
                'move_id': move_id.id,
                'account_id': line.purchase_property_id.partner_id.property_account_payable_id.id or False,
                'debit': 0.0,
                'credit': line.amount,
                'journal_id': journal_ids and journal_ids.ids[0],
                'partner_id': line.purchase_property_id.partner_id.id or False,
                'currency_id': company_currency != current_currency and current_currency or False,
                'amount_currency': company_currency != current_currency and - sign * line.amount or 0.0,
                'date': depreciation_date,
            })
            move_line_obj.create({
                'name': line.purchase_property_id.name,
                'ref': line.purchase_property_id.code,
                'move_id': move_id.id,
                'account_id': line.purchase_property_id.category_id.account_asset_id.id,
                'credit': 0.0,
                'debit': line.amount,
                'journal_id': journal_ids and journal_ids.ids[0],
                'partner_id': line.purchase_property_id.partner_id.id or False,
                'currency_id': company_currency != current_currency and current_currency,
                'amount_currency': company_currency != current_currency and sign * line.amount or 0.0,
                'analytic_account_id': line.purchase_property_id.analytic_acc_id.id or False,
                'date': depreciation_date,
                'asset_id': line.purchase_property_id.id or False,
            })
            line.write({'move_id': move_id.id})
            created_move_ids.append(move_id.id)
            move_id.write({'state': 'posted', 'ref': 'Purchase Installment'})
        return created_move_ids


class RoomAssets(models.Model):
    _name = "room.assets"

    date = fields.Date('Date')
    name = fields.Char('Description', size=60)
    type = fields.Selection([('fixed', 'Fixed Assets'),
                             ('movable', 'Movable Assets'),
                             ('other', 'Other Assets')
                             ], 'Type')
    qty = fields.Float('Quantity')
    room_id = fields.Many2one('property.room', string='Property')


class PropertyInsurance(models.Model):
    _name = "property.insurance"

    premium = fields.Float('Premium')
    end_date = fields.Date('End Date')
    doc_name = fields.Char('Filename')
    contract = fields.Binary('Contract')
    start_date = fields.Date('Start Date')
    name = fields.Char('Description', size=60)
    policy_no = fields.Char('Policy Number', size=60)
    contact = fields.Many2one('res.company', 'Insurance Comapny')
    company_id = fields.Many2one('res.company', 'Related Company')
    property_insurance_id = fields.Many2one('account.asset.asset', 'Property')
    payment_mode_type = fields.Selection(
        [('monthly', 'Monthly'), ('semi_annually', 'Semi Annually'), ('yearly', 'Annually')], 'Payment Term', size=40)


class TenancyRentSchedule(models.Model):
    _name = "tenancy.rent.schedule"
    _rec_name = "tenancy_id"
    _order = 'start_date'

    @api.one
    @api.depends('move_id')
    def _get_move_check(self):
        self.move_check = bool(self.move_id)

    note = fields.Text('Notes', help='Additional Notes.')
    currency_id = fields.Many2one(
        'res.currency', string='Currency', required=True)
    amount = fields.Monetary(
        default=0.0, string='Amount', currency_field='currency_id', help="Rent Amount.")
    start_date = fields.Date('Date', help='Start Date.')
    end_date = fields.Date('End Date', help='End Date.')
    cheque_detail = fields.Char('Cheque Detail', size=30)
    move_check = fields.Boolean(
        compute='_get_move_check', method=True, string='Posted', store=True)
    rel_tenant_id = fields.Many2one('tenant.partner', string="Tenant")
#   rel_tenant_id = fields.Many2one('tenant.partner', string="Tenant", ondelete='restrict',related='tenancy_id.tenant_id', store=True)
    move_id = fields.Many2one('account.move', 'Depreciation Entry')
    property_id = fields.Many2one(
        'account.asset.asset', 'Property', help='Property Name.')
    tenancy_id = fields.Many2one(
        'account.analytic.account', 'Tenancy', help='Tenancy Name.')
    paid = fields.Boolean('Paid', help="True if this rent is paid by tenant")
    invc_id = fields.Many2one('account.invoice', 'Invoice')
    pen_amt = fields.Float('Pending Amount', help='Pending Ammount...')

    @api.multi
    def open_invoice(self):
        context = dict(self._context or {})
        wiz_form_id = self.env['ir.model.data'].get_object_reference(
            'account', 'invoice_form')[1]
        return {
            'view_type': 'form',
            'view_id': wiz_form_id,
            'view_mode': 'form',
            'res_model': 'account.invoice',
            'res_id': self.invc_id.id,
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': context,
        }

    @api.multi
    def create_move(self):
        """
        This button Method is used to create account move.
        @param self: The object pointer
        """

        context = dict(self._context or {})
        move_line_obj = self.env['account.move.line']
        created_move_ids = []
        journal_ids = self.env['account.journal'].search(
            [('type', '=', 'sale')])
        for line in self:
            depreciation_date = datetime.now()
            company_currency = line.tenancy_id.company_id.currency_id.id
            current_currency = line.tenancy_id.currency_id.id
            sign = -1
            move_vals = {
                'name': line.tenancy_id.ref or False,
                'date': depreciation_date,
                'schedule_date': line.start_date,
                'journal_id': journal_ids and journal_ids.ids[0],
                'asset_id': line.tenancy_id.property_id.id or False,
                'source': line.tenancy_id.name or False,
            }
            move_id = self.env['account.move'].create(move_vals)
            if not line.tenancy_id.property_id.income_acc_id.id:
                raise Warning(
                    _('Please Configure Income Account from Property'))
            move_line_obj.create({
                'name': line.tenancy_id.name,
                'ref': line.tenancy_id.ref,
                'move_id': move_id.id,
                'account_id': line.tenancy_id.property_id.income_acc_id.id or False,
                'debit': 0.0,
                'credit': line.tenancy_id.rent,
                'journal_id': journal_ids and journal_ids.ids[0],
                'partner_id': line.tenancy_id.tenant_id.parent_id.id or False,
                'currency_id': company_currency != current_currency and current_currency or False,
                'amount_currency': company_currency != current_currency and - sign * line.tenancy_id.rent or 0.0,
                'date': depreciation_date,
            })
            move_line_obj.create({
                'name': line.tenancy_id.name,
                'ref': 'Tenancy Rent',
                'move_id': move_id.id,
                'account_id': line.tenancy_id.tenant_id.parent_id.property_account_receivable_id.id,
                'credit': 0.0,
                'debit': line.tenancy_id.rent,
                'journal_id': journal_ids and journal_ids.ids[0],
                'partner_id': line.tenancy_id.tenant_id.parent_id.id or False,
                'currency_id': company_currency != current_currency and current_currency,
                'amount_currency': company_currency != current_currency and sign * line.tenancy_id.rent or 0.0,
                'analytic_account_id': line.tenancy_id.id,
                'date': depreciation_date,
                'asset_id': line.tenancy_id.property_id.id or False,
            })
            line.write({'move_id': move_id.id})
            created_move_ids.append(move_id.id)
            move_id.write({'ref': 'Tenancy Rent', 'state': 'posted'})
        return created_move_ids


class PropertyUtility(models.Model):
    _name = "property.utility"

    note = fields.Text('Remarks')
    ref = fields.Char('Reference', size=60)
    expiry_date = fields.Date('Expiry Date')
    issue_date = fields.Date('Issuance Date')
    utility_id = fields.Many2one('utility', 'Utility')
    property_id = fields.Many2one('account.asset.asset', 'Property')
    tenancy_id = fields.Many2one('account.analytic.account', 'Tenancy')
    contact_id = fields.Many2one(
        'tenant.partner', 'Contact', domain="[('tenant', '=', True)]")


class PropertySafetyCertificate(models.Model):
    _name = "property.safety.certificate"

    ew = fields.Boolean('EW')
    weeks = fields.Integer('Weeks')
    ref = fields.Char('Reference', size=60)
    expiry_date = fields.Date('Expiry Date')
    name = fields.Char('Certificate', size=60, required=True)
    property_id = fields.Many2one('account.asset.asset', 'Property')
    contact_id = fields.Many2one(
        'tenant.partner', 'Contact', domain="[('tenant', '=', True)]")


class PropertyAttachment(models.Model):
    _name = 'property.attachment'

    doc_name = fields.Char('Filename')
    expiry_date = fields.Date('Expiry Date')
    contract_attachment = fields.Binary('Attachment')
    name = fields.Char('Description', size=64, requiered=True)
    property_id = fields.Many2one('account.asset.asset', 'Property')


class SaleCost(models.Model):
    _name = "sale.cost"
    _order = 'date'

    @api.one
    @api.depends('move_id')
    def _get_move_check(self):
        self.move_check = bool(self.move_id)

    date = fields.Date('Date')
    amount = fields.Float('Amount')
    name = fields.Char('Description', size=100)
    payment_details = fields.Char('Payment Details', size=100)
    currency_id = fields.Many2one('res.currency', 'Currency')
    move_id = fields.Many2one('account.move', 'Purchase Entry')
    sale_property_id = fields.Many2one('account.asset.asset', 'Property')
    remaining_amount = fields.Float(
        'Remaining Amount', help='Shows remaining amount in currency')
    move_check = fields.Boolean(
        compute='_get_move_check', method=True, string='Posted', store=True)
    rmn_amnt_per = fields.Float(
        'Remaining Amount In %', help='Shows remaining amount in Percentage')

    @api.multi
    def create_move(self):
        """
        This button Method is used to create account move.
        @param self: The object pointer
        """
        context = dict(self._context or {})
        move_line_obj = self.env['account.move.line']
        created_move_ids = []
        journal_ids = self.env['account.journal'].search(
            [('type', '=', 'sale')])
        for line in self:
            depreciation_date = datetime.now()
            company_currency = line.sale_property_id.company_id.currency_id.id
            current_currency = line.sale_property_id.currency_id.id
            sign = -1
            move_vals = {
                'name': line.sale_property_id.code or False,
                'date': depreciation_date,
                'journal_id': journal_ids and journal_ids.ids[0],
                'asset_id': line.sale_property_id.id or False,
                'source': line.sale_property_id.name or False,
            }
            move_id = self.env['account.move'].create(move_vals)
            if not line.sale_property_id.customer_id:
                raise Warning(_('Please Select Customer'))
            if not line.sale_property_id.income_acc_id:
                raise Warning(_('Please Select Income Account'))
            move_line_obj.create({
                'name': line.sale_property_id.name,
                'ref': line.sale_property_id.code,
                'move_id': move_id.id,
                'account_id': line.sale_property_id.income_acc_id.id or False,
                'debit': 0.0,
                'credit': line.amount,
                'journal_id': journal_ids and journal_ids.ids[0],
                'partner_id': line.sale_property_id.customer_id.id or False,
                'currency_id': company_currency != current_currency and current_currency or False,
                'amount_currency': company_currency != current_currency and - sign * line.amount or 0.0,
                'date': depreciation_date,
            })
            move_line_obj.create({
                'name': line.sale_property_id.name,
                'ref': line.sale_property_id.code,
                'move_id': move_id.id,
                'account_id': line.sale_property_id.customer_id.property_account_receivable_id.id or False,
                'credit': 0.0,
                'debit': line.amount,
                'journal_id': journal_ids and journal_ids.ids[0],
                'partner_id': line.sale_property_id.customer_id.id or False,
                'currency_id': company_currency != current_currency and current_currency,
                'amount_currency': company_currency != current_currency and sign * line.amount or 0.0,
                'analytic_account_id': line.sale_property_id.analytic_acc_id.id or False,
                'date': depreciation_date,
                'asset_id': line.sale_property_id.id or False,
            })
            line.write({'move_id': move_id.id})
            created_move_ids.append(move_id.id)
            move_id.write({'state': 'posted', 'ref': 'Sale Installment'})
        return created_move_ids
