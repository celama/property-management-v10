# -*- coding: utf-8 -*-
##############################################################################
#
#	OpenERP, Open Source Management Solution
#	Copyright (C) 2011-Today Serpent Consulting Services PVT LTD
#	(<http://www.serpentcs.com>)
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU Affero General Public License as
#	published by the Free Software Foundation, either version 3 of the
#	License, or (at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU Affero General Public License for more details.
#
#
#	You should have received a copy of the GNU Affero General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>.
############################################################################
from odoo import models, fields


class ResPartner(models.Model):
    _inherit = "res.partner"

    tenant = fields.Boolean(
        'Tenant', help="Check this box if this contact is a tenant.")
    occupation = fields.Char('Occupation', size=20)
    agent = fields.Boolean(
        'Agent', help="Check this box if this contact is a Agent.")


class ResUsers(models.Model):
    _inherit = "res.users"

    tenant_id = fields.Many2one('tenant.partner', 'Related Tenant')
    tenant_ids = fields.Many2many(
        'tenant.partner', 'rel_ten_user', 'user_id', 'tenant_id', 'All Tenents')
