from odoo import models, api


class PaymentParser(models.AbstractModel):
    _name = 'report.property_rent_report.report_rent_voucher_details'

    def get_amount(self, data):
        tot_amount = 0.0
        rent_amt = data.total_rent
        for line in data.rent_schedule_ids:
            if line.paid == True:
                tot_amount = tot_amount + line.amount
        return tot_amount

    def get_amount_due(self, data):
        tot_amount = 0.0
        tot_paid_amount = 0.0
        due_amt = 0.0
        upto_rent = data.rent
        for line in data.rent_schedule_ids:
            if line.paid == True:
                tot_paid_amount = tot_paid_amount + line.amount
                tot_amount = tot_amount + upto_rent
        due_amt = tot_amount - tot_paid_amount
        return due_amt

    @api.model
    def render_html(self, docids, data=None):
        Report = self.env['report']
        report = Report._get_report_from_name(
            'property_rent_report.report_rent_voucher_details')
        records = self.env['account.analytic.account'].browse(self.ids)
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': records,
            'data': data,
            'get_amount': self.get_amount,
            'get_amount_due': self.get_amount_due,
        }
        return Report.render('property_rent_report.report_rent_voucher_details', docargs)
