# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2011-Today Serpent Consulting Services PVT LTD.
#    (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
############################################################################

from odoo import models, fields, api,_
from dateutil.relativedelta import relativedelta
from datetime import datetime,date,timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.exceptions import UserError

class AccountAnalyticAccount(models.Model):
    _inherit = "account.analytic.account"

    penalty = fields.Float('Penalty (%)')
    penalty_day = fields.Integer('Penalty Count After Days')

#    @api.multi
#    @api.depends('penalty', 'penalty_day')
#    def check_all_penalty(self):
#        print"kp:::::::::::::::"
#        tenancy_obj = self.env['account.analytic.account']
#        tenancy_ids = tenancy_obj.search([])
#        tenancy_browse = tenancy_obj.browse(tenancy_ids.ids)
#        tenancy_rent_obj = self.env['tenancy.rent.schedule']
#        today_date = datetime.today().date()
#        for one_tenancy in tenancy_browse:
#            for one_payment_line in one_tenancy.rent_schedule_ids:
#                if not one_payment_line.paid:
#                    ten_date = datetime.strptime(one_payment_line.start_date,DEFAULT_SERVER_DATE_FORMAT).date()
#                    if one_payment_line.tenancy_id.penalty_day != 0:
#                        ten_date = ten_date + relativedelta(days=int(one_payment_line.tenancy_id.penalty_day))
#                    if ten_date < today_date:
#                        if (today_date - ten_date).days:
#                            line_amount_day = (one_payment_line.tenancy_id.rent * one_payment_line.tenancy_id.penalty) / 100
#                            print"line_amount_day:::::::::::",line_amount_day
#                            tenancy_rent_obj.write({'penalty_amount':line_amount_day})
#        return True


class TenancyRentSchedule(models.Model):
    _inherit = "tenancy.rent.schedule"
    _rec_name = "tenancy_id"
    _order = 'start_date'

    penalty_amount = fields.Float(compute='calculate_penalty', string='Penalty', store=True)

    @api.multi
    def create_invoice(self):
        """
        This Method is used to create invoice
        @param self: The object pointer
        """
#        tenancy_obj = self.env['account.analytic.account']
#        tenancy_ids = tenancy_obj.search([])
#        tenancy_browse = tenancy_obj.browse(tenancy_ids.ids)
#        today_date = datetime.today().date()
#        for one_tenancy in tenancy_browse:
#            for one_payment_line in one_tenancy.rent_schedule_ids:
#                print"one_payment_line:::::::::::::::::::", one_payment_line
#                if not one_payment_line.paid:
#                    ten_date = datetime.strptime(one_payment_line.start_date, DEFAULT_SERVER_DATE_FORMAT).date()
#                    print"ten_date not::::::::::::::::::", ten_date
#                    if one_payment_line.tenancy_id.penalty_day != 0:
#                        ten_date = ten_date + relativedelta(days=int(one_payment_line.tenancy_id.penalty_day))
#                        print"ten_date:::::::::::::::::::::::", ten_date
#                    if ten_date < today_date:
#                        if (today_date - ten_date).days:
#                            line_amount_day = (one_payment_line.tenancy_id.rent * one_payment_line.tenancy_id.penalty) / 100
#                            print"line_amount_day:::::::::::::", line_amount_day
##                            one_payment_line.penalty_amount = line_amount_day
#                            self.write({'penalty_amount': line_amount_day})

#        for data in self:
        inv_line_values = {
            'origin': 'tenancy.rent.schedule',
            'name': self.note or '',
            'quantity': 1,
            'account_id': self.tenancy_id.property_id.income_acc_id.id or False,
            'account_analytic_id': self.tenancy_id.id or False,
        }
        inv_values = {
            'partner_id': self.tenancy_id.tenant_id.parent_id.id or False,
            'payment_term_id' : self.tenancy_id.tenant_id.parent_id.property_payment_term_id.id or False,
            'type': 'out_invoice',
            'property_id': self.tenancy_id.property_id.id or False,
            'date_invoice': datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT) or False,
            'invoice_line_ids': [(0, 0, inv_line_values)],
        }
        if not inv_values['payment_term_id']:
            inv_values['date_due'] = inv_values['date_invoice']
        else:
            pterm = self.env['account.payment.term'].search([('id','=',inv_values['payment_term_id'])])
            pterm_list = pterm.with_context(currency_id=self.tenancy_id.currency_id.id).compute(value=1, date_ref=inv_values['date_invoice'])[0]
            inv_values['date_due'] = max(line[0] for line in pterm_list)
        if self.tenancy_id.currency_id != self.tenancy_id.secondary_currency_id:
            start_date = inv_values['date_due']+' 00:00:00'
            next_date = str(datetime.strptime(start_date,"%Y-%m-%d 00:00:00") + timedelta(days=1))
            currency = self.env['res.currency.rate'].search([('currency_id','=',self.tenancy_id.secondary_currency_id.id),('name','>=',start_date),('name','<',next_date)])
            if not currency:
                raise UserError(_("No rate is defined for date %s. Please add exchange rate and then try to make an invoice!"%(inv_values['date_due'])))
        if self.pen_amt == 0.00:
            self.calculate_penalty()
            amt = self.amount + self.penalty_amount
            if self.tenancy_id.currency_id != self.tenancy_id.secondary_currency_id:
                inv_line_values.update({'price_unit': currency.rate*amt or 0.00})
            else:
                inv_line_values.update({'price_unit': amt or 0.00})
        else:
            if self.tenancy_id.currency_id != self.tenancy_id.secondary_currency_id:
                inv_line_values.update({'price_unit': currency.rate*self.pen_amt or 0.00})
            else:
                inv_line_values.update({'price_unit': self.pen_amt or 0.00})
        acc_id = self.env['account.invoice'].create(inv_values)
        self.write({'invc_id': acc_id.id})

        context = dict(self._context or {})
        wiz_form_id = self.env['ir.model.data'].get_object_reference(
            'account', 'invoice_form')[1]
        return {
            'view_type': 'form',
            'view_id': wiz_form_id,
            'view_mode': 'form',
            'res_model': 'account.invoice',
            'res_id': self.invc_id.id,
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': context,
        }

    @api.multi
    @api.depends('tenancy_id.penalty', 'tenancy_id.penalty_day')
    def calculate_penalty(self):
        tenancy_obj = self.env['account.analytic.account']
        tenancy_browse = tenancy_obj.browse(self.tenancy_id.id)
        today_date = datetime.today().date()
        for one_payment_line in self:
            if not one_payment_line.paid:
                ten_date = datetime.strptime(one_payment_line.start_date, DEFAULT_SERVER_DATE_FORMAT).date()
                if one_payment_line.tenancy_id.penalty_day != 0:
                    ten_date = ten_date + relativedelta(days=int(one_payment_line.tenancy_id.penalty_day))
                if ten_date < today_date:
                    if (today_date - ten_date).days:
                        line_amount_day = (one_payment_line.tenancy_id.rent * one_payment_line.tenancy_id.penalty) / 100
                        self.write({'penalty_amount': line_amount_day})
        return True


class account_payment(models.Model):
    _inherit = 'account.payment'

    @api.multi
    def post(self):
        res = super(account_payment, self).post()
        tenancy = self.env['account.analytic.account']
        for data in tenancy.rent_schedule_ids.browse(self._context['active_id']):
            if data:
                tenan_rent_obj = self.env['tenancy.rent.schedule'].search(
                    [('invc_id', '=', data.id)])
                for data1 in tenan_rent_obj:
                    if data1.invc_id.state == 'paid':
                        data1.paid = True
                        data1.move_check = True
                    if data1.invc_id:
                        amt = data1.invc_id.residual
                    data1.write({'pen_amt': amt})
        return res
